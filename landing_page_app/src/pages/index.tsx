import Image from 'next/image';
import { Inter } from 'next/font/google';
import JoinNow from '../component/home/JoinNow';
import WhoWeAre from '../component/home/WhoWeAre';
import Analytics from '../component/home/Analytics';
import Testimonial from '../component/home/Testimonial';
import Footer from '@/component/home/Footer';
import Benefits from '@/component/home/Benefits';
const inter = Inter({ subsets: ['latin'] });
export default function Home() {
  return (
    <>
      <div className='antialiased'>
        <JoinNow></JoinNow>
        <div className='container px-4 lg:px-8 mx-auto max-w-screen-xl text-gray-700 overflow-y-hidden'>
          <WhoWeAre></WhoWeAre>
          <Analytics></Analytics>
          <Benefits></Benefits>
          <Testimonial></Testimonial>
        </div>
      </div>

      <Footer />
    </>
  );
}
