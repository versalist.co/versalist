import Link from 'next/link';
import React, { useEffect } from 'react';
const home_nav = [
  { title: 'Home', route: '/home' },
  { title: 'Test your speed', route: '/train' },
  { title: 'Library', route: '/library' },
];
import { useRouter } from 'next/router';
import AOS from 'aos';
import 'aos/dist/aos.css';

function Navbar(props: any) {
  const router = useRouter();
  useEffect(() => {
    AOS.init({ duration: 1000 });
  }, []);
  return (
    <>
      <div className='w-full text-gray-700 bg-primary200 sticky top-0 z-20'>
        <div className='flex flex-co items-center max-w-screen-xl px-8 mx-auto md:items-center md:justify-between md:flex-row'>
          <div className='flex flex-row items-center  py-6'>
            <div className='relative md:mt-8'>
              <a
                href='#'
                className='text-lg relative z- font-bold tracking-widest  rounded-lg focus:outline-none focus:shadow-outline text-white'
              >
                Versalist
              </a>
            </div>
          </div>
          <div
            data-aos='shake'
            data-aos-hover='tada'
            className='cursor-pointer mt-4  rounded-full px-4 py-2 bg-secondary300 text-white'
            onClick={() => {
              router.push('https://calendly.com/versalist/30min');
            }}
          >
            Book a meeting
          </div>
        </div>
      </div>
    </>
  );
}

export default Navbar;
