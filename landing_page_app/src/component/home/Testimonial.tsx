import React, { useEffect } from 'react';
import AOS from 'aos';
import 'aos/dist/aos.css';
import Image from 'next/image';

function Testimonial(props: any) {
  useEffect(() => {
    AOS.init({
      duration: 500,
    });
  }, []);
  return (
    <>
      <div className='pt-80 flex flex-col-reverse md:flex-row items-start md:space-x-10'>
        <div data-aos='zoom-in-right' className='w-full'>
          <div className='flex w- items-center space-x-20 mb-5'>
            <span className='border border-gray-300 w-1/3 '></span>
            <h1 className='text-gray-400 tracking-widest text-sm uppercase'>
              Latest clients
            </h1>
            <span className='border border-gray-300 w-1/3 '></span>
          </div>
          <div className='flex flex-col items-center justify-center '>
            <p className='text-gray-500 my-5 italic'>
              "Their passion for software development and attention to detail
              ensure that every project is delivered to the highest standard."
              <span className='not-italic'>-AtlantBH</span>
            </p>
          </div>

          <div className='flex justify-center gap-8'>
            <Image
              className='w-96 h-24  opacity-50 hover:opacity-100 transition duration-300 ease-in-out'
              src={'/images/atlant2.png'}
              width={400}
              height={100}
            />
            <Image
              className='w-24 h-24 opacity-50 hover:opacity-100 transition duration-300 ease-in-out'
              src={'/images/school_school.jpeg'}
              width={400}
              height={100}
            />
          </div>
        </div>
      </div>
    </>
  );
}

export default Testimonial;
