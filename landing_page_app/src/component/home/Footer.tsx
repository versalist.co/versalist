import React, { useEffect } from "react";

function Footer(props: any) {
  return (
    <>
      <footer className="mt-32 p-4 bg-primary200">
        <div className="max-w-lg mx-auto">
          <div className="flex py-12 justify-center text-white items-center px-20 sm:px-36">
            <div className="relative">
              <h1 className="font-bold text-xl pr-5 relative z-1">
                Versalist
              </h1>
            </div>
            <span className="border-l border-white text-sm pl-5 py-2 font-semibold">
             Softwear inc
            </span>
          </div>
          
          <div className="flex items-center text-white text-sm justify-center">
            <a href="" className="pr-3">
              About
            </a>
            <a href="" className="border-l border-white px-3">
              Privacy
            </a>
            <a href="" className="border-l border-white pl-3">
              Terms & Conditions
            </a>
          </div>
          <div className="text-center text-white">
            <p className="my-3 text-white text-sm">
              &copy; 2023 Versalist{" "}
            </p>
          </div>
        </div>
      </footer>
    </>
  );
}

export default Footer;
