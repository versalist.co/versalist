import React, { useEffect } from 'react';
import AOS from 'aos';
import 'aos/dist/aos.css';
import Image from 'next/image';
import { useRouter } from 'next/router';
function JoinNow(props: any) {
  useEffect(() => {
    AOS.init({
      duration: 500,
    });
  }, []);
  const router = useRouter();

  return (
    <>
      <div className='bg-primary200 pt-32'>
        <div className='max-w-screen-xl px-8 mx-auto flex flex-col lg:flex-row items-start p-8'>
          <div className='flex flex-col w-full lg:w-6/12 justify-center lg:pt-24 items-start text-center lg:text-left mb-5 md:mb-0'>
            <h1
              data-aos='fade-right'
              data-aos-once='true'
              className='my-4 text-5xl font-bold leading-tight text-white'
            >
              Making dreams come
              <span className='text-secondary100'> true </span>
            </h1>
            <p
              data-aos='fade-down'
              data-aos-once='true'
              data-aos-delay='300'
              className='leading-normal text-2xl mb-8 text-white'
            >
              Welcome to Versalist – the IT company started by a teenager with a
              dream to create software solutions for big companies!
            </p>
            <div
              data-aos='fade-up'
              data-aos-once='true'
              data-aos-delay='700'
              className='w-full md:flex items-center justify-center lg:justify-start md:space-x-5'
            >
              <button
                onClick={() => {
                  router.push('https://calendly.com/versalist/30min');
                }}
                className=' lg:mx-0 bg-primary300 hover:bg-primary400 text-white text-xl font-bold rounded-full py-4 px-9 focus:outline-none transform transition hover:scale-110 duration-300 ease-in-out'
              >
                Book a meeting
              </button>
            </div>
          </div>
          <div className='w-full lg:w-8/12 relative mt-5' id='girl'>
            <Image src={'images/home_run.svg'} width={400} height={100} />
          </div>
        </div>
        <div className='text-white -mt-12 sm:-mt-24 lg:-mt-12 z-1 relative'>
          <svg
            className='xl:h-40 xl:w-full'
            data-name='Layer 1'
            xmlns='http://www.w3.org/2000/svg'
            viewBox='0 0 1200 120'
            preserveAspectRatio='none'
          >
            <path
              d='M600,112.77C268.63,112.77,0,65.52,0,7.23V120H1200V7.23C1200,65.52,931.37,112.77,600,112.77Z'
              fill='currentColor'
            ></path>
          </svg>
          <div className='bg-white w-full h-20 -mt-px'></div>
        </div>
      </div>
    </>
  );
}

export default JoinNow;
