import React, { useEffect } from 'react';
import AOS from 'aos';
import 'aos/dist/aos.css';
import { BsLightbulbFill } from 'react-icons/bs';
import { RiComputerFill } from 'react-icons/ri';
import { FaMoneyCheck } from 'react-icons/fa';
import Image from 'next/image';
function Analytics(props: any) {
  useEffect(() => {
    AOS.init({
      duration: 500,
    });
  }, []);
  return (
    <>
      <div className='md:flex pt-80 md:space-x-10 items-start'>
        <div data-aos='fade-down' className='md:w-7/12 relative'>
          <div className='w-32 h-32 rounded-full absolute z-0 left-4 -top-12 animate-pulse bg-primary200'></div>
          <div className='w-5 h-5 rounded-full absolute z-0 left-36 -top-12 animate-ping bg-secondary200'></div>
          <Image
            className='relative z-50 floating'
            src='/images/how_we_work.png'
            width={400}
            height={100}
          />
          <div className='w-36 h-36 rounded-full absolute z-0 right-16 -bottom-3 animate-pulse bg-warning100'></div>
          <div className='w-5 h-5 rounded-full absolute  right-52 bottom-1 animate-ping bg-blue-300 z-50'></div>
        </div>
        <div
          data-aos='fade-down'
          className='md:w-5/12 mt-20 md:mt-0 text-gray-500'
        >
          <h1 className='text-2xl   lg:pr-40'>
            How we work{' '}
            <span className='text-primary200'>Versalist process</span>
          </h1>
          <div className='flex items-center space-x-5 my-5'>
            <div className='flex-shrink bg-white shadow-lg rounded-full p-3 flex items-center justify-center'>
              <BsLightbulbFill className='h5 text-primary200' />
            </div>
            <p>
              We turn client ideas into successful products by researching the
              market and ensuring a product fit.{' '}
            </p>
          </div>
          <div className='flex items-center space-x-5 my-5'>
            <div className='flex-shrink bg-white shadow-lg rounded-full p-3 flex items-center justify-center'>
              <RiComputerFill className='h5 text-secondary200' />
            </div>
            <p>
              We deliver high-quality software solutions by following a
              structured approach from idea to final product.
            </p>
          </div>
          <div className='flex items-center space-x-5 my-5'>
            <div className='flex-shrink bg-white shadow-lg rounded-full p-3 flex items-center justify-center'>
              <FaMoneyCheck className='h5 text-sucsses100' />
            </div>
            <p>
              We use CI/CD to ensure that our applications are accessible and
              easy to maintain, beyond just the initial build.
            </p>
          </div>
        </div>
      </div>
    </>
  );
}

export default Analytics;
