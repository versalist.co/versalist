import React, { useEffect } from 'react';
import Image from 'next/image';
function WhoWeARe(props: any) {
  return (
    <>
      <div className='mt-32'>
        <div
          data-aos='flip-down'
          className='text-center max-w-screen-md mx-auto'
        >
          <h1 className='text-3xl font-bold mb-4'>
            Latest <span className='text-primary200'>Projects</span>
          </h1>
          <p className='text-gray-500'>
            We have had the pleasure of working with two educational
            institutions and have successfully delivered two school management
            systems tailored to their specific needs. These systems have not
            only simplified their administrative tasks but have also provided a
            seamless experience to their staff, students, and parents.
          </p>
        </div>
        <div
          data-aos='fade-up'
          className='flex flex-col md:flex-row justify-center space-y-5 md:space-y-0 md:space-x-6 lg:space-x-10 mt-7'
        >
          <div className='relative md:w-5/12'>
            <Image
              width={400}
              height={400}
              className='bottom-0 left-0 right-0 w-full h-full rounded-2xl'
              src='/images/scholario.png'
            />

            <div className='absolute bg-black bg-opacity-20 bottom-0 left-0 right-0 w-full h-full rounded-2xl'>
              <div className='absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2'>
                <button className='rounded-full text-white text-xs lg:text-md px-6 py-3 w-full font-medium focus:outline-none transform transition hover:scale-110 duration-300 ease-in-out opacity-80 bg-primary200'>
                  Read more
                </button>
              </div>
            </div>
          </div>
          <div className='relative md:w-5/12'>
            <Image
              width={400}
              height={100}
              className='bottom-0 left-0 right-0 w-full h-full rounded-2xl'
              src='/images/school_school.png'
            />
            <div className='absolute bg-black bg-opacity-20 bottom-0 left-0 right-0 w-full h-full rounded-2xl'>
              <div className='absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2'>
                <button className='rounded-full text-white text-xs lg:text-md px-6 py-3 w-full font-medium focus:outline-none transform transition hover:scale-110 duration-300 ease-in-out opacity-80 bg-primary200'>
                  Read more
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default WhoWeARe;
